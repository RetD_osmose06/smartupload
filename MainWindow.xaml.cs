﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.Specialized;
using System.ServiceModel.Dispatcher;
using System.Diagnostics;
using System.IO;
using System.Windows.Threading;
using System.Windows.Forms;

namespace smartUpload
{

    public static class UiRefresh
    {

        private static Action EmptyDelegate = delegate () { };


        public static void Refresh(this UIElement uiElement)
        {
            uiElement.Dispatcher.Invoke(DispatcherPriority.Render, EmptyDelegate);
        }
    }

    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ProcessStartInfo pInfo;

        NameValueCollection sAll;
        public MainWindow()
        {
            InitializeComponent();
            txtBoxBatch.Text = MySettings.Default.lastBatch;
            txtBoxSerialNumber.Text = MySettings.Default.lastSerial;

            sAll = ConfigurationManager.AppSettings;

            getPorts();
            getProducts();
        }

        private void getPorts()
        {
            comboBoxComs.Items.Clear();
            foreach (string s in SerialPort.GetPortNames())
            {
                comboBoxComs.Items.Add(s);
            }
            string sAttr = ConfigurationManager.AppSettings.Get("Key0");
            NameValueCollection sAll;
            sAll = ConfigurationManager.AppSettings;

            //string xmlDataDirectory = ConfigurationSettings.AppSettings.Get("xmlDataDirectory");
        }

        private void getProducts()
        {
            foreach (var prod in sAll)
            {
                comboBoxProduits.Items.Add(prod.ToString());
                
            }

            //string xmlDataDirectory = ConfigurationSettings.AppSettings.Get("xmlDataDirectory");
        }

        private bool uploadESPCode()
        {

            string com = comboBoxComs.SelectedItem.ToString();
            string hexFile = sAll.Get(comboBoxProduits.SelectedItem.ToString());
            pInfo = new ProcessStartInfo("extras/python/3.7.2-post1/python.exe")
            {
                WorkingDirectory = @"extras/",
                //Arguments = "\"tools/upload.py\" --chip esp8266 --port \"" + com + "\" --baud \"115200\" version --end --chip esp8266 --port \"" + com + "\" --baud \"115200\" write_flash 0x0 \"" + hexFile + "\" write_flash 0xDB000 \"C:/Users/pierr/AppData/Local/Temp/VMBuilds/ESP8266-Websocket/esp8266_generic/Debug/1b7fa2be152fcdd9deb49f15372218e0/ESP8266-Websocket.ino.spiffs.bin\" --end",
                Arguments = "\"tools/upload.py\" --chip esp8266 --port \""+com+ "\" --baud \"115200\"  --after \"no_reset\" write_flash --flash_size 1MB 0x0 \"" + hexFile+ "\" --end",
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                CreateNoWindow = true,
                UseShellExecute = false
            };
            //        python\3.7.2-post1\python "C:\Users\pierr\AppData\Local\arduino15\packages\esp8266\hardware\esp8266\2.5.1/tools/upload.py" --chip esp8266 --port "COM13" --baud "115200" version --end --chip esp8266 --port "COM13" --baud "115200" write_flash 0x0 "C:\Users\pierr\AppData\Local\Temp\VMBuilds\ESP826~3\ESP826~1\Debug/ESP8266-Websocket.ino.bin" --end
            // --chip esp8266 --port COM13 --baud 115200 write_flash 0xDB000 "C:\Users\pierr\AppData\Local\Temp\VMBuilds\ESP8266-Websocket\esp8266_generic\Debug\1b7fa2be152fcdd9deb49f15372218e0/ESP8266-Websocket.ino.spiffs.bin" --end


            Process p = Process.Start(pInfo);

            string output = p.StandardOutput.ReadToEnd();
            string error = p.StandardError.ReadToEnd();

            // Are I assume that the second processing need to wait for the first to finish
            p.WaitForExit();
            

            /*"esptool.py v2.6\r\n
             * 2.6\r\nesptool.py v2.6\r\n
             * Serial port COM13\r\n
             * Connecting....\r\n
             * Chip is ESP8266EX\r\n
             * Features: WiFi\r\n
             * MAC: 84:0d:8e:a4:97:24\r\n
             * Uploading stub...\r\n
             * Running stub...\r\n
             * Stub running...\r\n
             * Configuring flash size...\r\n
             * Auto-detected Flash size: 1MB
             * \r\nCompressed 339456 bytes to 241984...\r\n\r
             * Writing at 0x00000000... (6 %)\rWriting at 0x00004000... (13 %)\rWriting at 0x00008000... (20 %)\rWriting at 0x0000c000... (26 %)\rWriting at 0x00010000... (33 %)\rWriting at 0x00014000... (40 %)\rWriting at 0x00018000... (46 %)\rWriting at 0x0001c000... (53 %)\rWriting at 0x00020000... (60 %)\rWriting at 0x00024000... (66 %)\rWriting at 0x00028000... (73 %)\rWriting at 0x0002c000... (80 %)\rWriting at 0x00030000... (86 %)\rWriting at 0x00034000... (93 %)\r
             * Writing at 0x00038000... (100 %)\r
             * Wrote 339456 bytes (241984 compressed) at 0x00000000 in 21.4 seconds (effective 127.1 kbit/s)...\r\n
             * Hash of data verified.\r\n\r\nLeaving...\r\n
             * Hard resetting via RTS pin...\r\n"
             */

            string token = "Wrote";
            int index = output.IndexOf(token);
            if (index < 0)
            {
                txtBoxSortie.Text = "ERROR:\n";
                txtBoxSortie.Text += error;
                txtBoxSortie.TextAlignment = TextAlignment.Left;
                return true;
            }
            return false;
        }




        private bool uploadCode()
        {
            string com = comboBoxComs.SelectedItem.ToString();
            string hexFile = sAll.Get(comboBoxProduits.SelectedItem.ToString());
            string verbose = "-v -V ";
            pInfo = new ProcessStartInfo("avrdude.exe")
            {
                WorkingDirectory = @"extras/",
                Arguments = "\"-Cavrdude.conf\" " + verbose + "-patmega2560 -cwiring \"-P" + com + "\" -b115200 -D \"-Uflash:w:\"" + hexFile + "\":i\"",
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                CreateNoWindow = true,
                UseShellExecute = false
            };

            Process p = Process.Start(pInfo);
            string output = p.StandardOutput.ReadToEnd();
            string error = p.StandardError.ReadToEnd();

            // Are I assume that the second processing need to wait for the first to finish
            p.WaitForExit();
            

            /*avrdude.exe: writing flash(41370 bytes):
             * Writing | ################################################## | 100% 6.63s
             * avrdude.exe: 41370 bytes of flash written
             */
            string token = "avrdude.exe: writing flash (";
            int index = error.IndexOf(token);
            if (index < 0)
            {
                txtBoxSortie.Text = "ERROR:\n";
                txtBoxSortie.Text += error;
                txtBoxSortie.TextAlignment = TextAlignment.Left;
                return true;
            }
            
            index = index + token.Length;
            int endIndex = error.IndexOf("bytes)", index);
            if (endIndex < 0)
            {
                txtBoxSortie.Text = "ERROR:\n";
                txtBoxSortie.Text += error;
                txtBoxSortie.TextAlignment = TextAlignment.Left;
                return true;
            }
            string substr = error.Substring(index, endIndex - index);

            int.TryParse(error.Substring(index, endIndex - index),out int size);

            token = "avrdude.exe: ";
            index = error.IndexOf(token, index);
            if (index < 0)
            {
                txtBoxSortie.Text = "ERROR:\n";
                txtBoxSortie.Text += error;
                txtBoxSortie.TextAlignment = TextAlignment.Left;
                return true;
            }

            index = index + token.Length;
            endIndex = error.IndexOf("bytes", index);
            if (endIndex < 0)
            {
                txtBoxSortie.Text = "ERROR:\n";
                txtBoxSortie.Text += error;
                txtBoxSortie.TextAlignment = TextAlignment.Left;
                return true;
            }
            substr = error.Substring(index, endIndex - index);

            int.TryParse(error.Substring(index, endIndex - index), out int size2);

            if (size == size2) return false;
            txtBoxSortie.Text = "ERROR:\n";
            txtBoxSortie.Text += error;
            txtBoxSortie.TextAlignment = TextAlignment.Left;
            return true;
                                 
        }

        private bool uploadEEPROM()
        {
            string eepromFile = readEEPROMFromFile();

            string com = comboBoxComs.SelectedItem.ToString();

            string verbose = "-v -V ";
            pInfo = new ProcessStartInfo("avrdude.exe")
            {
                WorkingDirectory = @"extras/",
                Arguments = "\"-Cavrdude.conf\" " + verbose + "-patmega2560 -cwiring \"-P" + com + "\" -b115200 -D -Ueeprom:w:"+ eepromFile + ":i",
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                CreateNoWindow = true,
                UseShellExecute = false
            };

            Process p = Process.Start(pInfo);
            string output = p.StandardOutput.ReadToEnd();
            string error = p.StandardError.ReadToEnd();

            // Are I assume that the second processing need to wait for the first to finish
            p.WaitForExit();

            /*
             * avrdude.exe: writing eeprom (4096 bytes):

Writing | ################################################## | 100% 16.78s

avrdude.exe: 4096 bytes of eeprom written
*/
            string token = "avrdude.exe: writing eeprom (";
            int index = error.IndexOf(token);
            if (index < 0)
            {
                txtBoxSortie.Text = "ERROR:\n";
                txtBoxSortie.Text += error;
                txtBoxSortie.TextAlignment = TextAlignment.Left;
                return true;
            }

            index = index + token.Length;
            int endIndex = error.IndexOf("bytes)", index);
            if (endIndex < 0)
            {
                txtBoxSortie.Text = "ERROR:\n";
                txtBoxSortie.Text += error;
                txtBoxSortie.TextAlignment = TextAlignment.Left;
                return true;
            }
            string substr = error.Substring(index, endIndex - index);

            int.TryParse(error.Substring(index, endIndex - index), out int size);

            token = "avrdude.exe: ";
            index = error.IndexOf(token, index);
            if (index < 0)
            {
                txtBoxSortie.Text = "ERROR:\n";
                txtBoxSortie.Text += error;
                txtBoxSortie.TextAlignment = TextAlignment.Left;
                return true;
            }

            index = index + token.Length;
            endIndex = error.IndexOf("bytes", index);
            if (endIndex < 0)
            {
                txtBoxSortie.Text = "ERROR:\n";
                txtBoxSortie.Text += error;
                txtBoxSortie.TextAlignment = TextAlignment.Left;
                return true;
            }
            substr = error.Substring(index, endIndex - index);

            int.TryParse(error.Substring(index, endIndex - index), out int size2);

            if (size == size2) return false;
            txtBoxSortie.Text = "ERROR:\n";
            txtBoxSortie.Text += error;
            txtBoxSortie.TextAlignment = TextAlignment.Left;
            return true;
        }

        private void ComboBoxProduits_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //txtBoxSortie.Text = sAll.Get(comboBoxProduits.SelectedItem.ToString());
            if (comboBoxProduits.SelectedItem.ToString().Contains("ESP8266"))
            {
                txtBoxBatch.Visibility = Visibility.Hidden;
                txtBoxSerialNumber.Visibility = Visibility.Hidden;
            }
            else
            {
                txtBoxBatch.Visibility = Visibility.Visible;
                txtBoxSerialNumber.Visibility = Visibility.Visible;
            }
        }

        private void BtnUpload_Click(object sender, RoutedEventArgs e)
        {
            if (comboBoxProduits.SelectedItem == null) txtBoxSortie.Text = "Sélectionnez un produit";
            else if (comboBoxComs.SelectedItem == null) txtBoxSortie.Text = "Sélectionnez un port COM";
            else
            {
                txtBoxSortie.Text = "Téléversement en cours...";

                MySettings.Default.lastBatch = txtBoxBatch.Text;
                MySettings.Default.lastSerial = txtBoxSerialNumber.Text;
                MySettings.Default.Save();
            }
            txtBoxSortie.TextAlignment = TextAlignment.Center;

            ((MainWindow)System.Windows.Application.Current.MainWindow).Refresh();
            ((MainWindow)System.Windows.Application.Current.MainWindow).UpdateLayout();   
        }

        private void uploadControllino()
        {
            try{
                bool error = uploadCode();
                if (!error) error = uploadEEPROM();
                if (!error)
                {
                    txtBoxSortie.Text = "Le téléversement s'est bien passé!";
                    txtBoxSortie.TextAlignment = TextAlignment.Center;
                    string log = comboBoxProduits.SelectedItem.ToString() + "." + txtBoxBatch.Text + "." + txtBoxSerialNumber.Text;
                    logToFile(log);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception Message: " + ex.Message);
                System.Windows.Forms.MessageBox.Show(
            ex.Message + "TRACE: " + ex.StackTrace,
            "Exception",
            MessageBoxButtons.OK,
            MessageBoxIcon.Error);
            }

        }

        private void uploadESP()
        {
            

            try
            {
                bool error = uploadESPCode();
                if (!error)
                {
                    txtBoxSortie.Text = "Le téléversement s'est bien passé!";
                    txtBoxSortie.TextAlignment = TextAlignment.Center;
                    string log = "ESP8266";
                    logToFile(log);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception Message: " + ex.Message);
                System.Windows.Forms.MessageBox.Show(
            ex.Message,
            "Exception",
            MessageBoxButtons.OK,
            MessageBoxIcon.Error);
            }
        }



        string readEEPROMFromFile()
        {

            string eepromFile = "EEPROM/blankEEPROM.hex";

            System.IO.StreamReader fileR = new System.IO.StreamReader("extras/"+ eepromFile);

            var line = fileR.ReadToEnd();
            fileR.Close();

            char[] c = line.ToCharArray();

            int.TryParse(txtBoxSerialNumber.Text, out int sfNumber);
            char b = txtBoxBatch.Text.ToCharArray()[0];

            char[] num = sfNumber.ToString("X4").ToCharArray();
            char[] bat = ((int)b).ToString("X2").ToCharArray();
            
            c[11] = num[2];
            c[12] = num[3];
            c[13] = num[0];
            c[14] = num[1];

            c[15] = bat[0];
            c[16] = bat[1];

            int csum = calculateChecksum(c);

            char[] sum = csum.ToString("X2").ToCharArray();
            c[73] = sum[0];
            c[74] = sum[1];

            line = new string(c);

            System.IO.StreamWriter file = new System.IO.StreamWriter("extras/" + eepromFile);
            file.Write(line);
            file.Close();

            return eepromFile;
        }

        int calculateChecksum(char[] ca)
        {
            int csum = 0;
            int size = 73;
            for (int i= 1;i< size; i+=2)
            {
                //char c = ca[i];
                string str = "";
                str += ca[i];
                str += ca[i+1];
                csum += int.Parse(str, System.Globalization.NumberStyles.HexNumber);
            }
            
            csum ^= 0xFF; // 1's complement
            csum++;
            csum = csum % 256;
            return csum;
        }
        bool readEEPROMFromArduino()
        {
            if (comboBoxComs.SelectedItem == null)
            {
                txtBoxSortie.Text = "Sélectionnez un port COM";
                return true;
            }
            string com = comboBoxComs.SelectedItem.ToString();

            string verbose = "-v -V ";
            pInfo = new ProcessStartInfo("avrdude.exe")
            {
                WorkingDirectory = @"extras/",
                Arguments = "\"-Cavrdude.conf\" " + verbose + "-patmega2560 -cwiring \"-P" + com + "\" -b115200 -D -Ueeprom:r:tempEEPROM.hex:i",
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                CreateNoWindow = true,
                UseShellExecute = false
            };

            Process p = Process.Start(pInfo);
            string output = p.StandardOutput.ReadToEnd();
            string error = p.StandardError.ReadToEnd();

            p.WaitForExit();
            txtBoxSortie.Text += "OUTPUT:\n";
            txtBoxSortie.Text = output;
            txtBoxSortie.Text += "\n\n";
            txtBoxSortie.Text += error;
            return false;
        }

        private void BtnDownloadEEPROM_Click(object sender, RoutedEventArgs e)
        {
            readEEPROMFromArduino();
        }

        private void TxtBoxSortie_TextChanged(object sender, TextChangedEventArgs e)
        {

            ((MainWindow)System.Windows.Application.Current.MainWindow).Refresh();
            ((MainWindow)System.Windows.Application.Current.MainWindow).UpdateLayout();
            if (txtBoxSortie.Text == "Téléversement en cours...")
            {
                if (comboBoxProduits.SelectedItem.ToString() == "ESP8266") uploadESP();
                else uploadControllino();
            }
        }

        private void ComboBoxComs_DropDownOpened(object sender, EventArgs e)
        {
            getPorts();
        }

        private void logToFile(string log)
        {
            string path = "Upload_log.txt";

            string line = "";
            line += DateTime.Now.ToString("MM/dd/yyyy HH:mm") + " :"+log;

            if (!File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine(line);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(path))
                {
                    sw.WriteLine(line);
                }
            }
        }
    }
}


